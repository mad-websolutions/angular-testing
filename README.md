# Unit Testing in Angular

Type of mocks
Dummy: plain simple object
Stub: object with controllable behavior 
spy: keep track which method is called
True mock: check if a specific method is only could and only once for example

type of tests:
isolated: (basic) unit test, e.g. give paramters by hand no deps
integration: - create a model (e.g. testing with template)
    shallow: only test it self
    deep: test itself and its child components
    
default tools:
- karma (runner)
- jasmine: (test suite)


good unit test:
structure: AAA pattern: arrange (setup/initial), act (changes), assert

example time:
isolated: see StrengthPipe


code coverage:
ng test --code-coverage