import {inject, TestBed} from '@angular/core/testing';
import {HeroService} from './hero.service';
import {MessageService} from './message.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

describe('HeroService', () => {
    let mockMessageService;
    let httpTestingController: HttpTestingController;
    let heroService: HeroService;

     beforeEach(() => {
         mockMessageService = jasmine.createSpyObj(['add']);

         TestBed.configureTestingModule({
             imports: [
                 HttpClientTestingModule,
             ],
             providers: [
                 HeroService,
                 {provide: MessageService, useValue: mockMessageService}
             ]
         });
         httpTestingController =  TestBed.get(HttpTestingController);
         heroService = TestBed.get(HeroService);
     });

     // This can be written with inject:
     describe ('getHero-inject', () => {
         it ('should get hero with the correct url', inject(
             [HeroService, HttpTestingController],
             (service: HeroService, controller: HttpTestingController
             ) => {
                 service.getHero(1).subscribe();
         }));
     });

     // Second method: through TestBed.get()
    describe ('getHero-testbed', () => {
        it ('should get hero with the correct url', () => {
                heroService.getHero(1).subscribe();

                const request = httpTestingController.expectOne('api/heroes/1');
                request.flush({id: 1, name: 'test', strength: 11});
                // verify that only the request above is done, not any other e.g. another with api/heroes/2
                httpTestingController.verify();
            });
    });

});
