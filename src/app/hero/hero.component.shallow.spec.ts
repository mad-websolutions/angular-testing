import {ComponentFixture, TestBed} from '@angular/core/testing';
import {HeroComponent} from './hero.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {By} from '@angular/platform-browser';

describe('HeroComponent (shallow)', () => {
    let fixture: ComponentFixture<HeroComponent>;
    let hero;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [HeroComponent],
            schemas: [NO_ERRORS_SCHEMA] // Do not error on unknown attribute or element but hides errors we wanna know!
        });
        fixture = TestBed.createComponent(HeroComponent);
        hero = {id: 1, name: 'first', strength: 11};
    });

    it ('should have the correct hero', () => {
        // arrange
        fixture.componentInstance.hero = hero;

        // act

        // assert
        expect(fixture.componentInstance.hero.name).toEqual('first');
    });

    it ('should render name in anchor tag', () => {
        // arrange
        fixture.componentInstance.hero = hero;
        fixture.detectChanges();

        // act

        // assert
        let aDebugElement = fixture.debugElement.query(By.css('A'));
        expect(aDebugElement.nativeElement.textContent).toContain(hero.name); // debug element (angular api)
        let aNativeElement = fixture.nativeElement.querySelector('A');
        expect(aNativeElement.textContent).toContain(hero.name); // native element (dom api)
    });
});

