import {MessageService} from '../message.service';

describe('MessageService', () => {
    let svc: MessageService;

    beforeEach(() => {
        // svc = new MessageService(); because of AAA pattern moved to each method
    });

    it('should not have msg at start', () => {
        // arrange
        svc = new MessageService();

        // act

        // assert
        expect(svc.messages.length).toBe(0);
    });


    it('should add a msg when add is called', () => {
        // arrange
        svc = new MessageService();

        // act
        svc.add('test');

        // assert
        expect(svc.messages.length).toBe(1);
    });


    it('should remove all msgs when clear is called', () => {
        // arrange
        svc = new MessageService();
        svc.add('test');

        // act
        svc.clear();

        // assert
        expect(svc.messages.length).toBe(0);
    });

});
