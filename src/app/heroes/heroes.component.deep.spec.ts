import {async, ComponentFixture, fakeAsync, flush, TestBed, tick} from '@angular/core/testing';
import {HeroesComponent} from './heroes.component';
import {HeroService} from '../hero.service';
import {of} from 'rxjs/internal/observable/of';
import {Directive, Input} from '@angular/core';
import {By} from '@angular/platform-browser';
import {HeroComponent} from '../hero/hero.component';

// stub for mocking the router
@Directive({
    selector: '[routerLink]',
    host: {'(click)' : 'onClick()'}
})
export class RouterLinkDirectiveStub {
    @Input('routerLink') linkParams: any;
    navigatedTo: any = null;

    onClick() {
        this.navigatedTo = this.linkParams;
    }
}

describe('HeroesComponent (deep)', () => {
    let fixture: ComponentFixture<HeroesComponent>;
    let mockHeroesService;
    let HEROES;

    beforeEach(() => {
        HEROES = [
            {id: 1, name: 'first', strength: 11},
            {id: 2, name: 'second', strength: 22},
            {id: 3, name: 'third', strength: 33},
        ];
        mockHeroesService = jasmine.createSpyObj(['getHeroes', 'addHero', 'deleteHero']);
        TestBed.configureTestingModule({
            declarations: [
                HeroesComponent,
                HeroComponent,
                RouterLinkDirectiveStub
            ],
            providers: [
                {provide: HeroService, useValue: mockHeroesService}
            ],
            // schemas: [NO_ERRORS_SCHEMA] // Do not error on unknown attribute or element but hides errors we wanna know!
        });
        fixture = TestBed.createComponent(HeroesComponent);
    });

    it ('should render each hero as a hero component', () => {
        // arrange
        mockHeroesService.getHeroes.and.returnValue(of(HEROES));

        // act
        fixture.detectChanges();

        // assert
        const heroComponentDebugElements = fixture.debugElement.queryAll(By.directive(HeroComponent));
        expect(heroComponentDebugElements.length).toBe(3);
        for (const key in heroComponentDebugElements) {
            expect(heroComponentDebugElements[key].componentInstance.hero).toBe(HEROES[key]);
        }
    });

    it ('should call heroService.deleteHero when the hero components delete button is clicked', () => {
        // arrange
        spyOn(fixture.componentInstance, 'delete');
        mockHeroesService.getHeroes.and.returnValue(of(HEROES));

        // act
        fixture.detectChanges();

        // assert
        const heroComponents = fixture.debugElement.queryAll(By.directive(HeroComponent));
        for (const key in heroComponents) {
            heroComponents[key].query(By.css('button')).triggerEventHandler('click', {stopPropagation: () => {}});
            // could also be called by emitting the event:
            (<HeroComponent>heroComponents[key].componentInstance).delete.emit(null);
            // or yet another (shorter) method using the debug elements trigger event
            heroComponents[key].triggerEventHandler('delete', null);
            expect(fixture.componentInstance.delete).toHaveBeenCalledWith(HEROES[key]);
        }
    });

    it ('should add a new hero to the list when the add button is clicked', () => {
        // arrange
        mockHeroesService.getHeroes.and.returnValue(of(HEROES));
        fixture.detectChanges();
        const name = 'fourth';
        const newHero = {id: 4, name: 'fourth', strength: 11};
        mockHeroesService.addHero.and.returnValue(of(newHero));
        const inputElement = fixture.debugElement.query(By.css('input')).nativeElement;
        const addButton = fixture.debugElement.queryAll(By.css('button'))[0];

        // act
        inputElement.value = name;
        addButton.triggerEventHandler('click', null);
        fixture.detectChanges();

        // assert
        const heroes = fixture.debugElement.queryAll(By.css('li a'));
        const heroText = heroes[heroes.length - 1].nativeElement.textContent;
        expect(heroText.trim()).toBe(  newHero.id + ' ' + newHero.name);
    });

    it ('should have the correct route for the first hero', () => {
        const heroComponentDebugElements = fixture.debugElement.queryAll(By.directive(HeroComponent));
        for (const key in heroComponentDebugElements) {
            const routerLink = heroComponentDebugElements[key]
                .query(By.directive(RouterLinkDirectiveStub))
                .injector.get(RouterLinkDirectiveStub);

            heroComponentDebugElements[key].query(By.css('a')).triggerEventHandler('click', null);

            expect(routerLink.navigatedTo).toBe('/detail/' + HEROES[key].id);
        }
    });

    // async: built in done, ugly but works but will reduce the unit test by 250 milisec
    it('testing done (works with callback method done()', (done) => {

        setTimeout(() => {
            expect(true).toBe(true);
            done();
        }, 250);
    });

    // best option for all async code tests
    it('testing fakeAsync (works with promises, timeouts and any other async code)', fakeAsync(() => {

        tick(250); // moves timeline 250 milisec in future, for testing timeouts
        flush(); // fast forward till after all async calls are done, for promises,  timeouts and any other async etc.
    }));

    // wont work timeouts, is made for promises
    it('testing async (promises)', async (() => {
        fixture.whenStable().then(() => {
            // will only be called when all promises are forfilled or rejected
            expect(true).toBe(true);
        });
    }));
});
