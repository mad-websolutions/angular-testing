import {HeroesComponent} from './heroes.component';
import {of} from 'rxjs/internal/observable/of';

describe('HeroesComponent', () => {
    let component: HeroesComponent;
    let HEROES;
    let mockHeroesSvc;

    beforeEach(() => {
        HEROES = [
            {id: 1, name: 'first', strength: 11},
            {id: 2, name: 'second', strength: 22},
            {id: 3, name: 'third', strength: 33},
        ];
        mockHeroesSvc = jasmine.createSpyObj(['getHeroes', 'addHero', 'deleteHero']);
        component = new HeroesComponent(mockHeroesSvc);
    });

    describe('delete', () => {
        it ('should remove the indicated heroes from the heroes list', () => {
            // arrange
            mockHeroesSvc.deleteHero.and.returnValue(of(true));
            component.heroes = HEROES;

            // act
            component.delete(HEROES[2]);

            // assert
            expect(component.heroes.length).toBe(2);
        });

        it ('should call deleteHero with correct hero', () => {
            // arrange
            mockHeroesSvc.deleteHero.and.returnValue(of(true));
            component.heroes = HEROES;

            // act
            component.delete(HEROES[2]);

            // assert
            expect(mockHeroesSvc.deleteHero).toHaveBeenCalledWith(HEROES[2]);
            // maybe complete with check if deleteHero has been subscribed to!
        });
    });
});
