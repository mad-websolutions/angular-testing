import {ComponentFixture, TestBed} from '@angular/core/testing';
import {HeroesComponent} from './heroes.component';
import {HeroService} from '../hero.service';
import {of} from 'rxjs/internal/observable/of';
import {Component, EventEmitter, Input, NO_ERRORS_SCHEMA, Output} from '@angular/core';
import {Hero} from '../hero';
import {by} from 'protractor';
import {By} from '@angular/platform-browser';

describe('HeroesComponent (shallow)', () => {
    let fixture: ComponentFixture<HeroesComponent>;
    let mockHeroesService;
    let HEROES;

    // Mock Hero component
    @Component({
        selector: 'app-hero',
        template: '<div></div>'
    })
    class FakeHeroComponent {
        @Input() hero: Hero;
        // @Output() delete = new EventEmitter();
    }

    beforeEach(() => {
        HEROES = [
            {id: 1, name: 'first', strength: 11},
            {id: 2, name: 'second', strength: 22},
            {id: 3, name: 'third', strength: 33},
        ];
        mockHeroesService = jasmine.createSpyObj(['getHeroes', 'addHero', 'deleteHero']);
        TestBed.configureTestingModule({
            declarations: [
                HeroesComponent,
                FakeHeroComponent
            ],
            providers: [
                {provide: HeroService, useValue: mockHeroesService}
            ],
            schemas: [NO_ERRORS_SCHEMA] // Do not error on unknown attribute or element but hides errors we wanna know!
        });
        fixture = TestBed.createComponent(HeroesComponent);
    });

    it ('should set heroes correctly from the heroes service', () => {
        // arrange
        mockHeroesService.getHeroes.and.returnValue(of(HEROES));
        fixture.detectChanges();

        // act


        // assert
        expect(fixture.componentInstance.heroes.length).toBe(3);
    });

    it ('should create one LI for each hero', () => {
        // arrange
        mockHeroesService.getHeroes.and.returnValue(of(HEROES));
        fixture.detectChanges();

        // act


        // assert
            expect(fixture.debugElement.queryAll(By.css('li')).length).toBe(3);
    });
});
