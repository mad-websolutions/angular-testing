import {StrengthPipe} from './strength.pipe';

describe('StrengtPipe', () => {
    it ('should return weak when the value is 9', () => {
        let pipe = new StrengthPipe();

        expect(pipe.transform(9)).toEqual('9 (weak)');
    });

    it ('should return strong when the value is 10', () => {
        let pipe = new StrengthPipe();

        expect(pipe.transform(10)).toEqual('10 (strong)');
    });
});
